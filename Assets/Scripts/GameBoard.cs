﻿using UnityEngine;
using System.Collections.Generic;

public class GameBoard
{
    private GameObject gamePiecePrefab;

    //The moves that can be taken with the current game state
    private List<BoardPosition> availableMoves;

    private Color[,] board;

    public GameBoard(GameObject gamePiecePrefab)
    {
        this.gamePiecePrefab = gamePiecePrefab;
        board = new Color[8, 8];
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                board[i, j] = Color.N;
            }
        }

        AddPiece(new BoardPosition(3, 3), Color.White, false);
        AddPiece(new BoardPosition(3, 4), Color.Black, false);
        AddPiece(new BoardPosition(4, 3), Color.Black, false);
        AddPiece(new BoardPosition(4, 4), Color.White, false);

        availableMoves = new List<BoardPosition>();
        GenerateAvailableMoves(Color.White);
    }

    public GameBoard(GameBoard other)
    {
        this.board = (Color[,]) other.board.Clone();
    }

    public void AddPiece(BoardPosition pos, Color color, bool simulated)
    {
        if(!simulated)
        {
            GameObject newPiece = (GameObject) GameObject.Instantiate(gamePiecePrefab, BoardPosition.BoardPositionToWorldSpace(pos), Quaternion.identity);
            newPiece.name = "piece" + pos.row + "" + pos.col;
            newPiece.GetComponentInChildren<GamePiece>().color = color;
            newPiece.transform.rotation = new Quaternion(0, 0, color == Color.White ? 180 : 0, 0);
        }
        board[pos.row, pos.col] = color;

        //Go through the surrounding pieces and flip any that need to be flipped
        for(int row = -1; row <= 1; row++)
        {
            for(int col = -1; col <= 1; col++)
            {
                if(row == 0 && col == 0)
                {
                    continue;
                }

                FlipPieces(pos.Clone(), row, col, color, simulated);
            }
        }
    }

    private void FlipPieces(BoardPosition start, int dRow, int dCol, Color color, bool simulated)
    {
        BoardPosition current = start.Clone();
               
        //Make sure there is another piece of the same color in this direciton
        while(true)
        {
            current.row += dRow;
            current.col += dCol;

            if(current.row < 0 || current.row >= 8 ||
               current.col < 0 || current.col >= 8)
            {
                return;
            }
            else if(board[current.row, current.col] == Color.N)
            {
                return;
            }
            else if(board[current.row, current.col] == color)
            {
                current.row -= dRow;
                current.col -= dCol;
                break;
            }
        }

        //Now go back and flip the pieces
        while(!current.Equals(start))
        {
            if(!simulated)
            {
                string pieceName = "piece" + current.row + current.col;
                GameObject.Find(pieceName).GetComponent<GamePiece>().Flip();
            }
            board[current.row, current.col] = color;

            current.row -= dRow;
            current.col -= dCol;
        }
    }

    public void GenerateAvailableMoves(Color color)
    {
        availableMoves = new List<BoardPosition>();

        List<BoardPosition> positions = GetColoredPiecePositions(color);
        foreach(BoardPosition pos in positions)
        {
            for(int row = -1; row <= 1; row++)
            {
                for(int col = -1; col <= 1; col++)
                {
                    //Dont check the current piece
                    if(row == 0 && col == 0)
                    {
                        continue;
                    }

                    BoardPosition p = FindAvailableMove(pos.Clone(), row, col, color);
                    if(p != null)
                    {
                        availableMoves.Add(p);
                    }
                }
            }
        }
    }

    private BoardPosition FindAvailableMove(BoardPosition currentPiece, int dRow, int dCol, Color color)
    {
        BoardPosition pos = null;
        Color opposite = color == Color.White ? Color.Black : Color.White;

        //Whether or not we've passed a piece of the opposite color
        bool passedOpposite = false;

        currentPiece.row += dRow;
        currentPiece.col += dCol;
        while(currentPiece.row >= 0 && currentPiece.row < 8 &&
              currentPiece.col >= 0 && currentPiece.col < 8)
        {
            Color currentColor = board[currentPiece.row, currentPiece.col];

            if(currentColor == color)
            {
                break;
            }
            else if(currentColor == opposite)
            {
                passedOpposite = true;
            }
            else
            {
                if(passedOpposite)
                {
                    pos = new BoardPosition(currentPiece.row, currentPiece.col);
                }

                break;
            }

            currentPiece.row += dRow;
            currentPiece.col += dCol;
        }

        return pos;
    }

    public int NumColoredPieces(Color color)
    {
        return GetColoredPiecePositions(color).Count;
    }

    private List<BoardPosition> GetColoredPiecePositions(Color color)
    {
        List<BoardPosition> positions = new List<BoardPosition>();

        for(int row = 0; row < 8; row++)
        {
            for(int col = 0; col < 8; col++)
            {
                if(board[row, col] == color)
                {
                    positions.Add(new BoardPosition(row, col));
                }
            }
        }

        return positions;
    }

    public bool Equals(GameBoard other)
    {
        for(int row = 0; row < 8; row++)
        {
            for(int col = 0; col < 8; col++)
            {
                if(board[row, col] != other.board[row, col])
                {
                    return false;
                }
            }
        }

        return true;
    }

    public Color[,] GetBoard()
    {
        return board;
    }

    public List<BoardPosition> GetAvailableMoves()
    {
        return availableMoves;
    }
}
