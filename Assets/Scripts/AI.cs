﻿using UnityEngine;
using System.Collections.Generic;

public class AI : MonoBehaviour
{
    private int depth; //How deep in the tree do we go?

    public AI(int depth)
    {
        this.depth = depth;
    }

    public void SetDepth(int depth)
    {
        this.depth = depth;
    }

    public BoardPosition GetMove(GameBoard board)
    {
        GameBoard testBoard = new GameBoard(board);
        testBoard.GenerateAvailableMoves(Color.Black);
        List<BoardPosition> moves = new List<BoardPosition>(testBoard.GetAvailableMoves());

        int bestVal = int.MinValue;
        int bestIndex = 0;

        for(int i = 0; i < moves.Count; i++)
        {
            BoardPosition move = moves[i];
            GameBoard newTestBoard = new GameBoard(testBoard);
            newTestBoard.AddPiece(move, Color.Black, true);

            int val = -Negamax(newTestBoard, depth - 1, -1);

            if(bestVal < val)
            {
                bestVal = val;
                bestIndex = i;
            }
        }

        if(moves.Count == 0)
        {
            return null;
        }
        else
        {
            return moves[bestIndex];
        }
    }

    //Based on the negamax pseudo-code found at https://en.wikipedia.org/wiki/Negamax
    private int Negamax(GameBoard board, int depth, int color)
    {
        Color myColor = color == 1 ? Color.Black : Color.White;
        Color theirColor = color == 1 ? Color.White : Color.Black;

        //See if there are any moves to be made
        bool noMoreMoves = false;

        board.GenerateAvailableMoves(myColor);
        if(board.GetAvailableMoves().Count == 0)
        {
            board.GenerateAvailableMoves(theirColor);
            noMoreMoves = board.GetAvailableMoves().Count == 0;
        }

        if(depth == 0 || noMoreMoves)
        {
            return EvaluateMove(board, myColor);
        }

        board.GenerateAvailableMoves(myColor);
        List<BoardPosition> moves = board.GetAvailableMoves();

        int bestVal = int.MinValue;
        foreach(BoardPosition pos in moves)
        {
            GameBoard testBoard = new GameBoard(board);
            testBoard.AddPiece(pos, myColor, true);
            int val = -Negamax(testBoard, depth - 1, -color);
            bestVal = Mathf.Max(bestVal, val);
        }

        return bestVal;
    }

    private int EvaluateMove(GameBoard board, Color curTurn)
    {
        Color opposite = curTurn == Color.White ? Color.Black : Color.White;
        int goodness = 0; //How good is the move?

        //How many more pieces do I have compared to them?
        goodness = board.NumColoredPieces(curTurn) - board.NumColoredPieces(opposite);

        goodness += NumInCorner(board, curTurn) * 6;
        goodness += NumOnEdge(board, curTurn) * 3;
        goodness += NumOnEdge(board, curTurn) * 2;

        //See if this was a game ending move
        board.GenerateAvailableMoves(curTurn);
        if(board.GetAvailableMoves().Count == 0)
        {
            board.GenerateAvailableMoves(opposite);
            if(board.GetAvailableMoves().Count == 0)
            {
                int myCount = board.NumColoredPieces(curTurn);
                int theirCount = board.NumColoredPieces(opposite);

                if(myCount > theirCount)
                {
                    goodness = int.MaxValue;
                }
                else if(theirCount > myCount)
                {
                    goodness = int.MinValue;
                }
                else
                {
                    goodness = int.MinValue;
                }
            }
        }

        return goodness;
    }

    private int NumInCorner(GameBoard board, Color color)
    {
        int count = 0;
        Color[,] b = board.GetBoard();

        if(b[0, 0] == color)
            count++;
        if(b[0, 7] == color)
            count++;
        if(b[7, 0] == color)
            count++;
        if(b[7, 7] == color)
            count++;

        return count;
    }

    private int NumOnEdge(GameBoard board, Color color)
    {
        int count = 0;
        Color[,] b = board.GetBoard();

        //Don't double count the corners
        for(int row = 1; row < 7; row++)
        {
            if(b[row, 0] == color)
                count++;
            if(b[row, 7] == color)
                count++;
        }
        for(int col = 1; col < 7; col++)
        {
            if(b[0, col] == color)
                count++;
            if(b[7, col] == color)
                count++;
        }

        return count;
    }

    private int NumOnDot(GameBoard board, Color color)
    {
        int count = 0;
        Color[,] b = board.GetBoard();

        if(b[2, 2] == color)
            count++;
        if(b[5, 2] == color)
            count++;
        if(b[2, 5] == color)
            count++;
        if(b[5, 5] == color)
            count++;
        if(b[1, 1] == color)
            count++;
        if(b[1,6] == color)
            count++;
        if(b[6, 1] == color)
            count++;
        if(b[6, 6] == color)
            count++;

        return count;
    }
}
