﻿using UnityEngine;

public class GamePiece : MonoBehaviour
{
    private const float FLOAT_EQUALITY_DIFF = 0.005f;
    private const float ANGLE_WHITE = 180;
    private const float ANGLE_BLACK = 0;

    public Color color;
    
    private float rotationChange = 0f;

    void Update()
    {
        gameObject.transform.Rotate(0, 0, rotationChange);
        //gameObject.transform.rotation = new Quaternion(0, 0, color == Color.White ? ANGLE_WHITE : ANGLE_BLACK, 0);

        float angle = 0f;
        Vector3 axis = new Vector3(0, 0, 1);
        gameObject.transform.rotation.ToAngleAxis(out angle, out axis);
        
        float desiredAngle = (color == Color.White) ? ANGLE_WHITE : ANGLE_BLACK;
        if(color == Color.Black && angle < FLOAT_EQUALITY_DIFF)
        {
            gameObject.transform.rotation = new Quaternion(0, 0, desiredAngle, 0);
            rotationChange = 0f;
        }
        else if(color == Color.White && desiredAngle - angle < FLOAT_EQUALITY_DIFF)
        {
            gameObject.transform.rotation = new Quaternion(0, 0, desiredAngle, 0);
            rotationChange = 0f;
        }
    }

    public void Flip()
    {
        //TODO Play the animaton isntead
  
        //gameObject.transform.rotation = new Quaternion(0, 0, color == Color.White ? ANGLE_WHITE : ANGLE_BLACK, 0);

        //Make it the opposite color
        if(color == Color.Black)
        {
            rotationChange = 2f;
            color = Color.White;
        }
        else
        {
            rotationChange = -2f;
            color = Color.Black;
        }
    }
}
