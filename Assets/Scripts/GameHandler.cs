﻿using UnityEngine;

public class GameHandler : MonoBehaviour
{
    private const int AI_MAX_LEVEL = 10;

    public GameObject gamePiecePrefab;
    public GameObject board;

    private AI ai;
    private GameBoard gameBoard;

    private int elapsedFrames; //Frames since animations started playing

    private bool gameOver;
    private bool humanTurn;

    private GUIStyle style;

    private float aiSliderVal;

    void Awake()
    {
        ai = new AI(3);
        gameBoard = new GameBoard(gamePiecePrefab);
        humanTurn = true;

        gameOver = false;

        style = new GUIStyle();
        style.fontSize = 50;

        aiSliderVal = 3;
    }

    void OnGUI()
    {
        Camera camera = Camera.main;

        //Draw the scores for each player
        style.fontSize = 50;
        GameObject whiteScoreObject = GameObject.Find("WhiteScorePiece");
        Vector3 screenPos = camera.WorldToScreenPoint(whiteScoreObject.transform.position);
        screenPos.y = Screen.height - screenPos.y;
        GUI.Label(new Rect(screenPos.x + 50, screenPos.y - 30, 100, 100), "" + gameBoard.NumColoredPieces(Color.White), style);

        GameObject blackScoreObject = GameObject.Find("BlackScorePiece");
        screenPos = camera.WorldToScreenPoint(blackScoreObject.transform.position);
        screenPos.y = Screen.height - screenPos.y;
        GUI.Label(new Rect(screenPos.x + 50, screenPos.y - 30, 100, 100), "" + gameBoard.NumColoredPieces(Color.Black), style);

        //Slider for AI difficulty
        style.fontSize = 20;
        GUI.Label(new Rect(50, Screen.height / 2 + 10, 100, 30), "AI Difficulty " + Mathf.FloorToInt(aiSliderVal), style);
        aiSliderVal = GUI.HorizontalSlider(new Rect(50, Screen.height / 2, 100, 30), aiSliderVal, 1, AI_MAX_LEVEL);
        ai.SetDepth(Mathf.FloorToInt(aiSliderVal));

        if(gameOver)
        {
            int diff = gameBoard.NumColoredPieces(Color.White) - gameBoard.NumColoredPieces(Color.Black);
            string message = "";
            if(diff > 0)
            {
                message = "YOU WIN";
            }
            else if(diff < 0)
            {
                message = "YOU LOSE";
            }
            else
            {
                message = "TIE";
            }

            //TODO Make this better
            style.fontSize = 50;
            GUI.Label(new Rect(10, 10, 300, 300), message, style);
        }
        else if(humanTurn)
        {
            style.fontSize = 30;
            GUI.Label(new Rect(0, 0, 300, 300), "Your turn", style);
        }
    }

    void Update()
    {
        int availableMoves = gameBoard.GetAvailableMoves().Count;

        if(!humanTurn)
        {
            elapsedFrames++;

            //Check if flipping animations have been finished before starting the AI
            if(elapsedFrames >= 90) //90 frames to rotate 180 degrees at 2 deg/frame
            {
                BoardPosition aiMove = ai.GetMove(gameBoard);

                //If the AI can't make a move, it's the players turn
                if(aiMove == null)
                {
                    gameBoard.GenerateAvailableMoves(Color.White);

                    humanTurn = true;
                    return;
                }
                gameBoard.AddPiece(aiMove, Color.Black, false);
                gameBoard.GenerateAvailableMoves(Color.White);


                elapsedFrames = 0;
                humanTurn = true;
            }
        }
        else
        {
            if(availableMoves == 0)
            {
                gameBoard.GenerateAvailableMoves(Color.Black);
                if(gameBoard.GetAvailableMoves().Count == 0)
                {
                    gameOver = true;
                }
                else
                {
                    humanTurn = false;
                }
            }
        }
    }

    void OnMouseDown()
    {
        //Don't waste time doing raycasting if it's not the human's turn
        if(!humanTurn || gameOver)
        {
            return;
        }

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(board.GetComponent<Collider>().Raycast(ray, out hit, Mathf.Infinity) &&
           Mathf.Abs(hit.point.x) < 4.0f && Mathf.Abs(hit.point.z) < 4.0f)
        {
            BoardPosition pos = BoardPosition.RaycastToBoardPosition(hit);
            foreach(BoardPosition p in gameBoard.GetAvailableMoves())
            {
                if(p.Equals(pos))
                {
                    gameBoard.AddPiece(pos, Color.White, false);
                    gameBoard.GenerateAvailableMoves(Color.Black);
                    humanTurn = false;
                    break;
                }
            }
        }
    }
}
