﻿using UnityEngine;

public class BoardPosition
{
    public int row;
    public int col;

    public BoardPosition(int row, int col)
    {
        this.row = row;
        this.col = col;
    }

    public bool Equals(BoardPosition other)
    {
        return this.row == other.row && this.col == other.col;
    }

    public BoardPosition Clone()
    {
        return new BoardPosition(this.row, this.col);
    }

    public static BoardPosition RaycastToBoardPosition(RaycastHit hit)
    {
        return new BoardPosition(Mathf.FloorToInt(hit.point.x + 4),
                                              Mathf.FloorToInt(4 - hit.point.z));
    }

    public static Vector3 BoardPositionToWorldSpace(BoardPosition pos)
    {
        Vector3 world = new Vector3();
        world.y = 0.48f;
        world.x = pos.row - 3.5f;
        world.z = 3.5f - pos.col;
        return world;
    }
}
